﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    [SerializeField] private Canvas targetCanvas;
    
    void Start()
    {
        Time.timeScale = 0;
    }
    

    public void StartGame()
    {
        Time.timeScale = 1;
        targetCanvas.enabled = !targetCanvas.enabled;
    }
    
}
