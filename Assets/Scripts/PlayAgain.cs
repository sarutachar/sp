﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    public class PlayAgain : MonoBehaviour
    {
        [SerializeField] private Text text;

        public void playAgain()
        {
            DestroyScore.enemyDestroyScore = 0;
            SceneManager.LoadScene("SampleScene");
        }
        
        

        void Update()
        {
            var temp = DestroyScore.enemyDestroyScore;
            text.text = "Score : " + temp;
        }
    }
}