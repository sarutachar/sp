﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private Text text;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void ScoreShow()
    {
        var temp = DestroyScore.enemyDestroyScore;
        text.text = "Score : " + temp;
    }

    void Update()
    {
        ScoreShow();
    }
}
