﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Transform bulletSpawn;
    public GameObject bulletCircle;

    public float bulletForce = 20f;

    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shooting();
        }
    }
    
    public void Shooting()
    {
        GameObject bullets = Instantiate(bulletCircle, bulletSpawn.position, bulletSpawn.rotation);
        Rigidbody2D rb = bullets.GetComponent<Rigidbody2D>();
        rb.AddForce(bulletSpawn.up*bulletForce,ForceMode2D.Impulse);
    }
}
