﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemy01;
    [SerializeField] private int spawnAmount;
    [SerializeField] private float spawnTime;
    private int spawnCount = 1;
    

    // Update is called once per frame
    void Update()
    {
        while (spawnCount <= spawnAmount)
        {
            Invoke("EnemySpawner",spawnTime);
            spawnCount++;
        }
    }

    public void EnemySpawner()
    {
        Instantiate(enemy01, transform.position, transform.rotation);
    }
}
