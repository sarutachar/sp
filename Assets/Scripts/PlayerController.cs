﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        private Vector2 movementInput = Vector2.zero;
        [SerializeField]
        
        // Start is called before the first frame update
        void Start()
        {
            Application.targetFrameRate = -1;
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            //OnMove();
        }

        private void Move()
        {
            movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            //Debug.Log(movementInput);
            
            var newX = transform.position.x + movementInput.x*Time.deltaTime*playerShipSpeed;
            //var newY = transform.position.y + movementInput.y*Time.deltaTime*playerShipSpeed;
            transform.position = new Vector2(newX, -4/*newY*/);
            
        }
        
/*
        public void OnMove()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var tempY = transform.position.y;
                var tempX = transform.position.x;
                transform.position = new Vector2(tempX-1,tempY);
            }
            if (Input.GetMouseButtonDown(1))
            {
                var tempY = transform.position.y;
                var tempX = transform.position.x;
                transform.position = new Vector2(tempX+1,tempY);
            }
        }*/
    }
}
