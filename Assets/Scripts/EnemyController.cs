﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D enemy;
    [SerializeField] private Rigidbody2D player;
    [SerializeField] private float speedAmp = 1;
    private Vector3 followVector;
    private Vector3 rotateVector;

    // Update is called once per frame
    void Update()
    {
        EnemyRotate();
        Move();
    }

    public void EnemyRotate()
    {
        followVector = player.position - enemy.position;
        rotateVector = -followVector;
        followVector.z = 0;
        rotateVector.z = 0;
        var angle = Mathf.Atan2(rotateVector.y, rotateVector.x) * Mathf.Rad2Deg - 90f;
        enemy.rotation = angle;
    }

    public void Move()
    {
        enemy.velocity = followVector * speedAmp;
    }

    
}
