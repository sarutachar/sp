﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObj : MonoBehaviour
{
    [SerializeField] private Canvas endGameCanvas;
    [SerializeField] public float timeMax;
    public float timeCount = 0f;
    private bool isEnd = false;
    
    
    void Update()
    {
        timeCount += 1 * Time.deltaTime;
        EndGame();
    }

    public void EndGame()
    {
        if (timeCount >= timeMax && isEnd == false)
        {
            Time.timeScale = 0;
            endGameCanvas.enabled = !endGameCanvas.enabled;
            isEnd = true;
        }
    }
}
