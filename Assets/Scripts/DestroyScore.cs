﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class DestroyScore : MonoBehaviour
    {
        public static int enemyDestroyScore;

        public void UpdateScore(int Score)
        {
            enemyDestroyScore = Score;
        }

        public int ShowScore()
        {
            return enemyDestroyScore;
        }
    
        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("enemy"))
            {
                enemyDestroyScore++;
                Debug.Log(enemyDestroyScore);
                Destroy(other.gameObject);
            }
        }
        
        

        public void OnCollisionEnter2D(Collision2D destroyBullet)
        {
        
            Destroy(gameObject);
        }

        
    }
}

