﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroy : MonoBehaviour
{
    public static int enemyScore;
    
    
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "BulletCircle(Clone)" || other.gameObject.CompareTag("wall"))
        {
            enemyScore++;
            Debug.Log(enemyScore);
            Destroy(this);
        }
    }

}
